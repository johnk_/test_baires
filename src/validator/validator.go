package validator

import "strings"
const (
	parensOpen        = "("
	parensClose       = ")"
	bracketsOpen      = "["
	bracketsClose     = "]"
	curlyBracesOpen  = "{"
	curlyBracesClose = "}"
)
func ValidateString(text string) bool {
	sliceText := strings.Split(text, "")
	var validateLine []string
	for _, c := range sliceText {
		if c == parensOpen || c == bracketsOpen || c == curlyBracesOpen {
			validateLine = append(validateLine, c)
		}
		if len(validateLine)>0 {
			switch c {
			case parensClose:
				if validateLine[len(validateLine)-1] == parensOpen {
					validateLine = validateLine[:len(validateLine)-1]
				}
				break
			case bracketsClose:
				if validateLine[len(validateLine)-1] == bracketsOpen {
					validateLine = validateLine[:len(validateLine)-1]
				}
				break
			case curlyBracesClose:
				if validateLine[len(validateLine)-1] == curlyBracesOpen {
					validateLine = validateLine[:len(validateLine)-1]
				}
				break
			}
		}
	}
	if len(validateLine)>0{
		return false
	}
	return true
}

