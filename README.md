# Executable example:

[https://play.golang.org/p/LjxxtA4PszJ](https://play.golang.org/p/LjxxtA4PszJ)

## Exercise
Write a function that returns true if the brackets in a given string are balanced. The function must handle parens (), square brackets [], and curly braces {}.

## Test Cases
- `(a[0]+b[2c[6]]) {24 + 53}` -> true
- `f(e(d))` -> true
- `[()]{}([])` -> true
- `((b)` -> false
- `(c]` -> false
- `{(a[])` -> false
- `([)]` -> false
- `)(` -> false
- ` ` -> true
