package test

import (
	"testing"
	"validator"
)

func TestValidateString(t *testing.T) {
	type args struct {
		text string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test (a[0]+b[2c[6]]) {24 + 53}",
			args: args{
				text: "(a[0]+b[2c[6]]) {24 + 53}",
			},
			want: true,
		},
		{
			name: "test f(e(d)) ",
			args: args{
				text: "f(e(d))",
			},
			want: true,
		},
		{
			name: "test [()]{}([])",
			args: args{
				text: "[()]{}([])",
			},
			want: true,
		},
		{
			name: "test ((b) ",
			args: args{
				text: "((b) ",
			},
			want: false,
		},
		{
			name: "test  (c] ",
			args: args{
				text: "(c]",
			},
			want: false,
		},
		{
			name: "test  {(a[]) ",
			args: args{
				text: "{(a[])",
			},
			want: false,
		},
		{
			name: "test  ([)] ",
			args: args{
				text: "([)]",
			},
			want: false,
		},
		{
			name: "test  )( ",
			args: args{
				text: ")(",
			},
			want: false,
		},
		{
			name: "test  empty ",
			args: args{
				text: "",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := validator.ValidateString(tt.args.text); got != tt.want {
				t.Errorf("ValidateString() = %v, want %v", got, tt.want)
			}
		})
	}
}
